$( ".bot-fornecedor" ).click(function() {
  $( ".escolha" ).animate({
    right: "20%",
    opacity: 0
  }, 500, function() {
  	$('.escolha').hide();
  } );
  $(".fornecedor").css("display", "inline-block").delay(600);
  $( ".fornecedor" ).animate({
    left: "0%",
    opacity: 1
  }).delay(500);
});

$( ".bot-viavarejo" ).click(function() {
  $( ".escolha" ).animate({
    right: "20%",
    opacity: 0
  }, 500, function() {
  	$('.escolha').hide();
  } );
  $(".viavarejo").css("display", "inline-block").delay(600);
  $( ".viavarejo" ).animate({
    left: "0%",
    opacity: 1
  }).delay(500);
});

$( ".seta" ).click(function() {
  $( ".seta" ).parent().animate({
    left: "20%",
    opacity: 0
  }, 500, function() {
  	$( ".seta" ).parent().hide();
  } );
  $(".escolha").css("display", "inline-block").delay(600);
  $( ".escolha" ).animate({
    right: "0%",
    opacity: 1
  }).delay(500);
});

$( ".politica" ).click(function() {
  $('.dark').fadeIn();
	$('.lightbox-wrapper').delay(400).fadeIn();
  console.log("oi");
});

$( ".fechar" ).click(function() {
  $('.lightbox-wrapper').fadeOut();
  $('.dark').delay(400).fadeOut();
});